% % Charger les donn es du fichier double_tapMat.txt
 data1 = load('double_tapMat.txt');
 data2 = load('fistMat.txt');
% Afficher le premier signal dans une fen tre graphique
 figure;
 subplot(2, 1, 1);
 plot(data1(:, 1), (data1(:, 2)));
 title('Signal du fichier double_tapMat.txt');
 xlabel('Temps');
 ylabel('Amplitude');
 hold on;

% Afficher le deuxi me signal dans une fen tre graphique
subplot(2, 1, 2);
plot(data2(:, 1), data2(:, 4));
title('Signal du fichier fistMat.txt');
xlabel('Temps');
ylabel('Amplitude');
axis tight;
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%% en abs
 figure;
 subplot(2, 1, 1);
 plot(data1(:, 1), abs(data1(:, 2)),'r');
 title('Signal du fichier double_tapMat.txt en abs ');
 xlabel('Temps');
 ylabel('Amplitude');
 hold on;

% Afficher le deuxi me signal dans une fen tre graphique
subplot(2, 1, 2);
plot(data2(:, 1), abs(data2(:, 2)),'r');
title('Signal du fichier fistMat.txt en abs');
xlabel('Temps');
ylabel('Amplitude');
axis tight;

figure

for c=2:9
    t=data1(:,1);
    subplot(8,1,c-1)
    
    plot(t,data1(:,c))
end
figure

for c=2:9
    t=data2(:,1); 
    subplot(8,1,c-1)
    
    plot(t,data2(:,c))
end


data11=data1(:, 2:end);
data22=data2(:, 2:end);

std_data1 = std(data11); % ecart-type de data1
std_data2 = std(data22); % ecart-type de data2

figure;
scatter(1:numel(std_data1), std_data1, 50, 'b', 'filled'); % Scatter plot pour data1 (en bleu)
hold on;
scatter(1:numel(std_data2), std_data2, 50, 'r', 'filled');% Scatter plot pour data2 (en rouge)



