# Analyse de Signaux

Ce script MATLAB vise à analyser deux ensembles de signaux à partir des fichiers `double_tapMat.txt` et `fistMat.txt`. Voici une explication du code :

## Chargement des Données
```matlab
% Charger les données du fichier double_tapMat.txt
data1 = load('double_tapMat.txt');
data2 = load('fistMat.txt');
```
Les données sont chargées à partir des fichiers texte. data1 contient les données du fichier double_tapMat.txt et data2 celles de fistMat.txt.

**Afficher le premier signal dans une fenêtre graphique**
```

figure;
subplot(2, 1, 1);
plot(data1(:, 1), (data1(:, 2)));
title('Signal du fichier double_tapMat.txt');
xlabel('Temps');
ylabel('Amplitude');
hold on;
```


**Afficher le deuxième signal dans une fenêtre graphique**
```
subplot(2, 1, 2);
plot(data2(:, 1), data2(:, 4));
title('Signal du fichier fistMat.txt');
xlabel('Temps');
ylabel('Amplitude');
axis tight;
```
Deux graphiques sont créés pour afficher les signaux bruts. Le premier affiche les données de double_tapMat.txt, et le second celles de fistMat.txt.

**Afficher les signaux en valeur absolue**
````
figure;
subplot(2, 1, 1);
plot(data1(:, 1), abs(data1(:, 2)), 'r');
title('Signal du fichier double_tapMat.txt en abs');
xlabel('Temps');
ylabel('Amplitude');
hold on;

subplot(2, 1, 2);
plot(data2(:, 1), abs(data2(:, 2)), 'r');
title('Signal du fichier fistMat.txt en abs');
xlabel('Temps');
ylabel('Amplitude');
axis tight;
````
Ces blocs de code affichent les signaux en valeur absolue. Le premier subplot montre abs(data1(:, 2)) du fichier double_tapMat.txt, et le deuxième affiche abs(data2(:, 2)) de fistMat.txt.

# Afficher les signaux individuels
````
figure;
for c = 2:9
    t = data1(:, 1);
    subplot(8, 1, c-1);
    plot(t, data1(:, c));
end

figure;
for c = 2:9
    t = data2(:, 1);
    subplot(8, 1, c-1);
    plot(t, data2(:, c));
end
````
Ces parties du code affichent les signaux individuels des colonnes 2 à 9 pour chaque fichier.

## Calcul de l'écart-type des Données
````
data11 = data1(:, 2:end);
data22 = data2(:, 2:end);

std_data1 = std(data11); % écart-type de data1
std_data2 = std(data22); % écart-type de data2
````

Les écarts-types des données sont calculés pour chaque colonne, à partir de la deuxième colonne jusqu'à la fin.


## Affichage des Écarts-Types

````
figure;
scatter(1:numel(std_data1), std_data1, 50, 'b', 'filled'); % Scatter plot pour data1 (en bleu)
hold on;
scatter(1:numel(std_data2), std_data2, 50, 'r', 'filled'); % Scatter plot pour data2 (en rouge);
````
Enfin, un scatter plot est créé pour afficher les écarts-types calculés. Les valeurs de std_data1 sont en bleu et celles de std_data2 en rouge.

Vous pouvez exécuter ce script MATLAB en ayant les fichiers double_tapMat.txt et fistMat.txt dans le même répertoire. Cela générera les graphiques et analyses décrits ci-dessus.